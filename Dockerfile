# Use a Rust base image
FROM rust:latest as builder

WORKDIR /usr/src/app

# Copy the local Cargo.toml and Cargo.lock files to the container
COPY Cargo.toml Cargo.lock ./

# Build dependencies to speed up the build process
RUN mkdir src && echo "fn main() {}" > src/main.rs && cargo build --release

# Copy the entire source code to the container
COPY . .

RUN cargo build --release

# Use a slim ubuntu image for the final container
FROM ubuntu:latest

WORKDIR /usr/src/app

# Copy the built binary from the builder image
COPY --from=builder /usr/src/app/target/release/actix_web_app .

EXPOSE 8088

CMD ["/usr/src/app/actix_web_app"]
