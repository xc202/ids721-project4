use actix_web::{web, App, HttpServer, Responder, HttpResponse};

async fn hello() -> impl Responder {
    HttpResponse::Ok().body("Hello, this is XiuYuan!")
}

async fn post_handler(req: String) -> impl Responder {
    println!("{:?}", req);
    let response = format!("Received POST request: {}", req);
    HttpResponse::Ok().body(response)
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .service(web::resource("/").to(hello))
            .service(web::resource("/post").route(web::post().to(post_handler)))
    })
        .bind("0.0.0.0:8088")?
        .run()
        .await
}

