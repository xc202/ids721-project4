## Actix Web Rust Service

This mini-project was designed to build a simple rust actix web application, with Docker making it to be containerized and running in the container locally. **Rust** (https://www.rust-lang.org/) and **Docker** (https://www.docker.com/) are required to start this project. 

### 1. Rust Code Overview
The `hello` function is an asynchronous handler that returns an HTTP response with a body containing the message "Hello, this is XiuYuan!" when accessed.

The post_handler function is an asynchronous handler for POST requests. It prints the incoming request and constructs a response indicating that a POST request was received, including the content of the request.

The main function sets up and runs the Actix Web server. It binds the server to the address "0.0.0.0:8088" and defines two routes:
* The root ("/") route, which maps to the hello function.
* The "/post" route, which maps to the post_handler function for POST requests.

### 2. Docker Overview
Docker is a platform for building, deploying, and running applications in containers. Containers are lightweight, portable, and consistent environments that encapsulate an application and its dependencies. Docker uses a client-server architecture, where the Docker client communicates with the Docker daemon to manage containers.

#### Dockerfile Explanation:
The Dockerfile first builds the dependencies to speed up the process and then builds the application itself. The final image is based on a slim Ubuntu image, and it copies the built binary into the container, exposing port 8088 and defining the command to run the application. The following steps show the details:
* Specify the base image as the latest Rust image and names it '**builder**'.
* Set the working directory within the container to `/usr/src/app`.
* Copy the local `Cargo.toml` and `Cargo.lock` files into the container.
* Build the application within the container in release mode.
* Change the base image to the latest **slim** Ubuntu image for the final container.
* Set the working directory within the final container to `/usr/src/app`.
* Copy the built binary from the '**builder**' image to the final container.
* Inform Docker that the application inside the container will listen on port **8088**.
* Specify the command to run the application when the container starts.

### 3. Build Docker Image
Since the Dockerfile has been defined, we can now use the command `docker build -t actix_web_app .` to build the image based on the Dockerfile. The code in Dockerfile is resolved line by line. The size of the final image is much less than which of the latest Rust image: 

![Screenshot 2024-02-22 at 10.05.37 AM.png](screenshot%2FScreenshot%202024-02-22%20at%2010.05.37%20AM.png)

I am using a Docker Desktop for Mac. It provides better visualization to see the images and containers created. We can see that an image `actix_web_app` is created after the pre-mentioned command is run. This image contains the basic environment for the web app to run and is slight for deployment. 

![Screenshot 2024-02-22 at 4.58.52 AM.png](screenshot%2FScreenshot%202024-02-22%20at%204.58.52%20AM.png)

### 4. Run the Container Locally
We can start using the command `docker run -d -p 8088:8088 actix_web_app` to build a container based on the image `actix_web_app` in the detached mode. The web application is providing service on the port `8088` in the container, mapping 8088 port of the container to 8088 port of the host, allowing users to access the website locally: 

![Screenshot 2024-02-22 at 4.59.25 AM.png](screenshot%2FScreenshot%202024-02-22%20at%204.59.25%20AM.png)

Now that we can run the container `quirky_mirzakhani`, and the web application should be listening on http://localhost:8088. The following figure shows what happens when accessing the website:

![Screenshot 2024-02-22 at 5.00.22 AM.png](screenshot%2FScreenshot%202024-02-22%20at%205.00.22%20AM.png)
